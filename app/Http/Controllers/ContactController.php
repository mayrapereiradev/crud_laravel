<?php

namespace App\Http\Controllers;

use App\Contact;
use App\Http\Requests\ContactRequest;
use Illuminate\Http\Request;

class ContactController extends Controller
{
    public function index()
    {
        $contacts = Contact::paginate(5);

        return view('contacts.index', [
            'contacts' =>$contacts,
        ]);
    }

    public function create()
    {

        return view('contacts.create');
    }

    public function store(ContactRequest $request)
    {
        $data = $request->all();

        $data['telephone'] = str_replace( array( '\'', '"', 
        ',' , ';', '<', '>', '-', '(', ')', ' '), '', $data['telephone']); 

        if( $request->hasFile('file_path')){
            $data['file_path'] = $data['file_path']->store('files', 'public');
        }

        $contact = new Contact();

        $contact->create($data);

        session()->flash('message', 'Contato criado com sucesso.');

        return redirect()->route('contact.index');

    }

    public function update(ContactRequest $request, $id)
    {
        $data = $request->all();

        $data['telephone'] = str_replace( array( '\'', '"', 
        ',' , ';', '<', '>', '-', '(', ')', ' '), '', $data['telephone']); 

        $contact = Contact::find($id);

        if( $request->hasFile('file_path')){
            $data['file_path'] = $data['file_path']->store('files', 'public');
        }

        $contact->update($data);

        session()->flash('message', 'Contato editado com sucesso.');

        return redirect()->route('contact.index');

        
    }

    public function edit($id)
    {
        $contact = Contact::find($id);
   
        return view('contacts.edit', [
            'contact' =>$contact,
        ]);


    }

    public function destroy($id)
    {

        $contact = Contact::find($id);
        $contact->delete();

        flash('Contato removido com sucesso!')->success();

        return redirect()->route('contact.index');

    }


    public function photoDelete(Request $request)
    {
       $contact = Contact::find($request['contact']);
       $contact->update(['file_path' => null]);

       flash('Foto removida com sucesso.!')->success();

       return redirect()->route('contact.edit', [
        'id' =>$contact->id,
       ]);

    }

}
