<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'ContactController@index')->name('contact.index');
Route::get('/create', 'ContactController@create')->name('contact.create');
Route::post('/store', 'ContactController@store')->name('contact.store');
Route::get('/edit/{id}', 'ContactController@edit')->name('contact.edit');
Route::put('/update/{id}', 'ContactController@update')->name('contact.update');
Route::delete('/destroy/{id}', 'ContactController@destroy')->name('contact.destroy');
Route::post('/photo/delete', 'ContactController@photoDelete')->name('contact.photo.delete');


