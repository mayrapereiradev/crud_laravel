@extends('layouts.front')

@section('content')

    <div class="container">
        <h1 class="title">Lista de Contatos</h1>
        <div class="wrapper">
            @if (session()->has('message'))
            <div class="alert"> {{ session()->get('message') }}</div>
        @endif
        <a href="{{ route('contact.create') }}" class="btn btn-sm mt-1 mb-3 btn-focus-new btn-hover-new button-new">Novo
            Contato</a>
        @if($contacts->count() > 0)

        <table class="table table-borderless">
            <thead>
                <tr>
                    <th scope="col">Nome</th>
                    <th scope="col">Email</th>
                    <th scope="col">Telefone</th>
                    <th scope="col">Ações</th>
                </tr>
            </thead>
            <tbody>
                @foreach ($contacts as $contact)
                    <tr>
                        <td>{{ $contact->name }}</td>
                        <td>{{ $contact->email }}</td>
                        <td class="telephone">{{ $contact->telephone }}</td>
                        <td>
                            <div class="btn-group">
                                <a href="{{ route('contact.edit', ['id' => $contact->id]) }}"
                                    data-toggle="modal" data-target="#modalContact" 
                                    class="btn btn-sm mr-2 btn-focus-fa btn-hover-fa button-fa"><i class="fa fa-eye"></i></a>
                            </div>
                            <div class="btn-group">
                                <a href="{{ route('contact.edit', ['id' => $contact->id]) }}"
                                    class="btn btn-sm mr-2 btn-focus-edit btn-hover-edit button-edit">Editar</a>
                            </div>
                            <div class="btn-group">
                                <form action="{{ route('contact.destroy', ['id' => $contact->id]) }}" method="post">
                                    @csrf
                                    @method("DELETE")
                                    <button class="btn btn-sm btn-focus-delete btn-hover-delete button-delete"
                                        type="submit">Remover</button>
                                </form>
                            </div>
                        </td>
                    </tr>
                @endforeach
        </table>

        {{ $contacts->links() }}

        @else
        <h3 class="alert">
            Nenhum contato foi cadastrado.
        </h3>
        @endif
        </div>    
    </div>

    <div class="modal fade" id="modalContact" tabindex="-1" role="dialog" aria-labelledby="modalContactLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
          <div class="modal-content">
            <div class="modal-header">
            <h5 class="modal-title" id="modalContactLabel">{{$contact->name}}</h5>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="modal-body">
              <ul>
              <li>Telefone : {{$contact->telephone}}</li>
              <li>Email : {{$contact->email}}</li>
              <li>Menssagem : {{$contact->message}}</li>
              </ul>
              @if($contact->file_path != '')
              <div class="col-10 mt-3 ml-4">
                <div class="image-area">
                    <img src="{{ asset('storage/'.$contact->file_path)  }}"  alt="Preview" class="img-fluid">
                </div>
              </div>
              @else
                <ul>
                  <li>Nenhuma foto cadastrada.</li>
                </ul>
              @endif
            </div>
            <div class="modal-footer">
                <a href="{{ route('contact.index') }}" data-dismiss="modal"  class="btn btn-sm btn-focus-delete btn-hover-delete button-delete">Voltar</a>
            </div>
          </div>
        </div>
      </div>

    @section('scripts')
        <script>
            $(document).ready(function() {
                $('.telephone').mask('(00) 00000-0000');
            });

        </script>
    @endsection
@endsection
