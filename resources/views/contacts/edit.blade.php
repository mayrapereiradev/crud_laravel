@extends('layouts.front')

@section('content')

    <div class="container">
        <h1 class="title">Novo Contato</h1>
        <div class="wrapper animated bounceInLeft">
            <div class="contact">
                @if (session()->has('message'))
                    <div class="alert"> {{ session()->get('message') }}</div>
                @endif
                <form id="contactForm" action="{{ route('contact.update', ['id' => $contact->id]) }}" method="post" enctype="multipart/form-data">
                    @csrf
                    @method("PUT")
                    <p class="name-field">
                        <label>Nome <span>*</span></label>
                        <input type="text" name="name" id="name" required value="{{ $contact->name }}">
                        @error('name')
                            <label class="error-contact">
                                {{ $message }}
                            </label>
                        @enderror
                    </p>
                    <p class="email-field">
                        <label>Email <span>*</span></label>
                        <input type="email" name="email" id="email" required value="{{ $contact->email }}">
                        @error('email')
                            <label class="error-contact">
                                {{ $message }}
                            </label>
                        @enderror
                    </p>
                    <p class="phone-field">
                        <label>Telefone <span>*</span></label>
                        <input type="text" name="telephone" id="telephone" required value="{{ $contact->telephone }}">
                        @error('telephone')
                            <label class="error-contact">
                                {{ $message }}
                            </label>
                        @enderror
                    </p>
                    <p class="message-field full">
                        <label>Messagem <span>*</span></label>
                        <textarea name="message" rows="5" id="message" required ">{{ $contact->message }}</textarea>
                        @error('message')
                            <label class="error-contact">
                                {{ $message }}
                            </label>
                        @enderror
                        </p>
                    <p class=" file-field full">
                        <label>Selecione uma fotografia (png, jpeg ou jpg)</label>
                        <input type="file" name="file_path" id="file" >
                        @error('file_path')
                            <label class="error-contact">
                                {{ $message }}
                            </label>
                        @enderror
                    </p>
                    <p class="required-field">Campo Obrigatório<span>*</span></p>
                    <p class="submit-button">
                        <a href="{{ route('contact.index') }}" class="btn btn-sm btn-focus-delete btn-hover-delete button-delete">Voltar</a>
                        <a href="" class="btn btn-sm btn-focus-modal btn-hover-modal button-modal" data-toggle="modal" data-target="#modalPhoto">Foto Atual</a>
                        <button class="contact-button" type="submit">Salvar</button>
                    </p>
                </form>
            </div>
        </div>
    </div>

    @section('scripts')
        <script>
            $(document).ready(function() {
                $('#telephone').mask('(00) 00000-0000');
            });

        </script>
    @endsection

    <div class="modal fade" id="modalPhoto" tabindex="-1" role="dialog" aria-labelledby="modalPhotoLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
          <div class="modal-content">
            <div class="modal-body">
                <div class="col-10 mt-3 ml-4">
                    @if($contact->file_path != '')
                        <div class="image-area">
                            <img src="{{ asset('storage/'.$contact->file_path)  }}"  alt="Preview" class="img-fluid">
                            <a class="remove-image" data-toggle="modal" data-target="#modalDeletePhoto" href="#" style="display: inline;">&#215;</a>
                        </div>
                    @else
                        Nenhuma foto encontrada.
                    @endif
                </div>
                <div class="modal-footer">
                    <a href="{{ route('contact.index') }}" data-dismiss="modal"  class="btn btn-sm btn-focus-delete btn-hover-delete button-delete">Voltar</a>
                </div>
            </div>
          </div>
        </div>
      </div>    


      <div class="modal fade" id="modalDeletePhoto" tabindex="-1" role="dialog" aria-labelledby="modalDeletePhotoLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
          <div class="modal-content">
            <div class="modal-header">
              <h5 class="modal-title" id="modalDeletePhotoLabel">Excluir Foto</h5>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="modal-body">
              Quer exluir foto atual?
            </div>
            <div class="modal-footer">
            <form action="{{route('contact.photo.delete')}}" method="POST">
                    @csrf
                    <input type="hidden" name="contact" value="{{$contact->id}}">
                    <button type="submit" class="btn btn-sm btn-focus-new btn-hover-new button-new">Excluir Foto</button>
                    <a href="{{ route('contact.index') }}" data-dismiss="modal"  class="btn btn-sm btn-focus-delete btn-hover-delete button-delete">Voltar</a>
                </form>
            </div>
          </div>
        </div>
      </div>


@endsection

